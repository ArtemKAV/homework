'''
2) Реализуйте модуль  word_utils.py, позволяющий:

- очистить предложение от знаков препинания;

- получить список слов из предложения;

- получить самое длинное слово в предложении;
'''

import word_utils

some_sentence = input('Enter sentence with punctuation marks: ')

# очистить предложение от знаков препинания;
print('Sentence clear of punctuation marks; ', word_utils.clear_a_sentence_of_punctuation_marks(some_sentence))

# получить список слов из предложения;
print('Word list: ', word_utils.create_words_list(some_sentence))

# получить самое длинное слово в предложении;
print('Max length word: ', word_utils.max_len_word_from_sentence(some_sentence))
