'''
1) Напишите программу, которая будет считывать содержимое файла, добавлять к считанным строкам порядковый
номер и сохранять их в таком виде в новом файле. Имя исходного файла необходимо запросить у пользователя,
так же, как и имя целевого файла. Каждая строка в созданном файле должна начинаться с ее номера, двоеточия и пробела,
 после чего должен идти текст строки из исходного файла.
'''


def read_file_create_file(read_file, create_file):
    read_file = input('Enter path and file name, to read data: ')
    create_file = input('Enter file name to enter datat: ')
    try:
        with open(read_file, 'r') as f:
            strings = f.read().splitlines()

        with open(create_file, 'w') as f1:
            for i in range(len(strings)):
                if len(strings[i]) > 0:
                    f1.write(str(i + 1) + ': ' + strings[i] + '\n')
    except OSError:
        print('Probable, some file name was wrong!')
    else:
        print('Success!')
