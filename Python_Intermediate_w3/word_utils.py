import string


def clear_a_sentence_of_punctuation_marks(sentence):    # очистить предложение от знаков препинания;
    new_sentence = ''

    for i in sentence:
        if i not in string.punctuation:
            new_sentence += i
    return new_sentence


def create_words_list(sentence):                        # получить список слов из предложения;
    return clear_a_sentence_of_punctuation_marks(sentence).split()


def max_len_word_from_sentence(sentence):               # получить самое длинное слово в предложении;
    maximum = ''
    for i in create_words_list(sentence):
        if len(i) > len(maximum):
            maximum = i
    return maximum
