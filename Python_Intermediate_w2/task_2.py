# 2) Определите класс итератор ReverseIter, который принимает список и итерируется по нему в обратном направлении


class ReverseIter:
    def __init__(self, iter_list):
        self.iter_list = iter_list

    def __iter__(self):
        self.element_index = -1
        return self

    def __next__(self):
        if self.element_index + len(self.iter_list) >= 0:
            list_item = self.iter_list[self.element_index]
            self.element_index -= 1
            return list_item
        else:
            raise StopIteration


iterable_list = ReverseIter([1, 2, 3, 4, 5, 6, 7, 8])

for i in iterable_list:
    print(i)

