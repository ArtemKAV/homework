'''
1) Необходимо написать функцию калькулятор, которая принимает строку состоящую из числа, оператора и второго числа
    разделенных пробелом. Например ('1 + 1'); Необходимо разделить строку используя str.split(), и проверить является
    результирующий список валидным.

    a) Если ввод не состоит из 3 элементов, необходимо возбудить исключение FormulaError,
    которое является пользовательским исключением.

    b) Попытайтесь сконвертировать первое и третье значение ввода к типу float.
    Перехватите любые исключения типа ValueError, которые возникают, и выбросите FormulaError

    c) Если второе значение ввода не является '+', '-', '*', '/' также выбросите
    FormulaError.
    Если инпут валидный - ф-я должна вернуть результат операции
'''


class FormulaError(Exception):
    def __init__(self, message='Error in your string'):
        self.message = message
        super().__init__(self.message)



def calculator(strng):
    calculate_list = strng.split()

    if len(calculate_list) != 3:                            # a)
        raise FormulaError(f'Your input does not have 3 elements, it have {len(calculate_list)} elements')
    elif calculate_list[1] not in ['+', '-', '/', '*']:     # c)
        raise FormulaError(f"The second input value is '{calculate_list[1]}' it must be  '+', '-', '*' or '/'")

    try:                                                    # b)
        first_num = float(calculate_list[0])
        second_num = float(calculate_list[2])
    except ValueError:
        raise FormulaError('You can input only elements which can convert in float.')
    else:
        return eval(strng)


print(calculator(input('Enter through the space what you need to count: ')))
