'''
3) Определить функцию-генератор fib_generator(n), которая принимает количество элементов последовательности
    Фибоначчи и итерируется по элементам последовательности.

    например fib_generator(3) создаст итератор для 3 элементов последовательности 0 1 1
    Чи́сла Фибона́ччи — элементы числовой последовательности
    0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765, 10946, 17711, …,
    в которой первые два числа равны 0 и 1, а каждое последующее число равно сумме двух предыдущих чисел

    Написать подобную ф-ю fib_list(n) которая возвращает список с элементами последовательности.

    Вызов ф-и fib_list(3) вернет список [0, 1, 1]
'''


# Определить функцию-генератор fib_generator(n)
def fib_generator(n):
    current_num = 0
    next_num = 1
    try:
        if n > 0:
            for j in range(n):
                if j == 0:
                    yield j
                else:
                    current_num, next_num = next_num, next_num + current_num
                    yield current_num
        else:
            raise ValueError(f'Only positive integers are allowed, you entered {n}')
    except TypeError:
        print(f'Only integers are allowed, you entered {n} - {type(n)}')


# Написать подобную ф-ю fib_list(n) которая возвращает список с элементами последовательности.
def fib_list(n):
    fib_nums = []

    if isinstance(n, int):
        if n > 0:
            if n == 1:
                fib_nums.append(0)
            elif n == 2:
                fib_nums.extend([0, 1])
            else:
                fib_nums = [0, 1]
                while len(fib_nums) < n:
                    fib_nums.append(fib_nums[-1] + fib_nums[-2])
            return fib_nums
        else:
            raise ValueError(f'Only positive integers are allowed, you entered {n}')
    else:
        raise TypeError(f'Only integers are allowed, you entered {n} - {type(n)}')


a = fib_generator(8)

for i in a:
    print(i)

print(fib_list(8))

